# Utiliza una imagen base de OpenJDK
FROM openjdk:17
# Establece el directorio de trabajo
WORKDIR /app
# Copia el archivo JAR de la aplicación al contenedor
COPY ./component/magnetron-app-0.0.1-SNAPSHOT.jar .

# Expone el puerto en el que se ejecuta la aplicación Spring Boot
EXPOSE 8888

# Comando para ejecutar la aplicación cuando el contenedor se inicia
CMD ["java", "-jar", "magnetron-app-0.0.1-SNAPSHOT.jar"]