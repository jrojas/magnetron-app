package com.magnetron.app.magnetronapp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.magnetron.app.magnetronapp.controllers.ProductoController;
import com.magnetron.app.magnetronapp.dto.ProductoRequest;
import com.magnetron.app.magnetronapp.dto.ProductoResponse;
import com.magnetron.app.magnetronapp.entities.Producto;
import com.magnetron.app.magnetronapp.exceptions.ExcepcionApi;
import com.magnetron.app.magnetronapp.services.IProductoService;

@SpringBootTest
@AutoConfigureMockMvc
public class ProductoControllerTest {

	@InjectMocks
	private ProductoController productoController;

	@Mock
	private IProductoService productoService;

	@Mock
	private ModelMapper modelMapper;

	@Test
	public void testCreateProducto() throws ExcepcionApi {
		ProductoRequest productoRequest = new ProductoRequest();
		ProductoResponse productoResponse = new ProductoResponse();
		when(productoService.save(productoRequest)).thenReturn(new Producto());
		when(modelMapper.map(any(), eq(ProductoResponse.class))).thenReturn(productoResponse);
		ResponseEntity<Object> response = productoController.createProducto(productoRequest);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals(productoResponse, response.getBody());
	}

	@Test
	public void testGetProducts() {
		List<Producto> productos = Arrays.asList(new Producto(), new Producto());
		when(productoService.findAll()).thenReturn(productos);
		when(modelMapper.map(any(), eq(ProductoResponse.class))).thenReturn(new ProductoResponse());
		ResponseEntity<List<ProductoResponse>> response = productoController.getProducts();
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(productos.size(), response.getBody().size());
	}

	@Test
	public void testDeletePerson() {
		Long idProduct = 1L;
		ResponseEntity<Void> response = productoController.deleteProduc(idProduct);
		verify(productoService, times(1)).delete(idProduct);
		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}

}
