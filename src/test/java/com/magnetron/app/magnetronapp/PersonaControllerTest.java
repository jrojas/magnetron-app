package com.magnetron.app.magnetronapp;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.magnetron.app.magnetronapp.controllers.PersonaController;
import com.magnetron.app.magnetronapp.dto.PersonaRequest;
import com.magnetron.app.magnetronapp.dto.PersonaResponse;
import com.magnetron.app.magnetronapp.entities.Persona;
import com.magnetron.app.magnetronapp.exceptions.ExcepcionApi;
import com.magnetron.app.magnetronapp.services.IPersonaService;

@SpringBootTest
@AutoConfigureMockMvc
public class PersonaControllerTest {

	@InjectMocks
	private PersonaController personaController;

	@Mock
	private IPersonaService personaService;

	@Mock
	private ModelMapper modelMapper;

	@Test
	public void testCreatePersona() throws ExcepcionApi {
		PersonaRequest personaRequest = new PersonaRequest();
		PersonaResponse personaResponse = new PersonaResponse();
		when(personaService.save(personaRequest)).thenReturn(new Persona());
		when(modelMapper.map(any(), eq(PersonaResponse.class))).thenReturn(personaResponse);
		ResponseEntity<Object> response = personaController.createPersona(personaRequest);
		assertEquals(HttpStatus.CREATED, response.getStatusCode());
		assertEquals(personaResponse, response.getBody());
	}

	@Test
	public void testGetPersons() {

		List<Persona> personas = Arrays.asList(new Persona(), new Persona());
		when(personaService.findAll()).thenReturn(personas);
		when(modelMapper.map(any(), eq(PersonaResponse.class))).thenReturn(new PersonaResponse());
		ResponseEntity<List<PersonaResponse>> response = personaController.getPersons();
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertEquals(personas.size(), response.getBody().size());
	}

	@Test
	public void testDeletePerson() {

		Long idPerson = 1L;
		ResponseEntity<Void> response = personaController.deletePerson(idPerson);
		verify(personaService, times(1)).delete(idPerson);
		assertEquals(HttpStatus.NO_CONTENT, response.getStatusCode());
	}

}
