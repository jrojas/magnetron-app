package com.magnetron.app.magnetronapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MagnetronAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(MagnetronAppApplication.class, args);
	}

}
