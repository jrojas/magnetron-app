package com.magnetron.app.magnetronapp.controllers;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.magnetron.app.magnetronapp.dto.MessageResponse;
import com.magnetron.app.magnetronapp.dto.ProductoRequest;
import com.magnetron.app.magnetronapp.dto.ProductoResponse;
import com.magnetron.app.magnetronapp.entities.Producto;
import com.magnetron.app.magnetronapp.exceptions.ExcepcionApi;
import com.magnetron.app.magnetronapp.services.IProductoService;

@RestController
@RequestMapping(value = "api/productos")
public class ProductoController {

	@Autowired
	IProductoService productoService;

	@Autowired
	ModelMapper modelMapper;

	@RequestMapping(method = RequestMethod.POST, value = "")
	public ResponseEntity<Object> createProducto(@Valid @RequestBody ProductoRequest producto) {
		try {
			ProductoResponse per = modelMapper.map(productoService.save(producto), ProductoResponse.class);
			return new ResponseEntity<>(per, HttpStatus.CREATED);
		} catch (ExcepcionApi e) {
			return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("")
	@RequestMapping(method = RequestMethod.GET, value = "")
	public ResponseEntity<List<ProductoResponse>> getProducts() {
		List<ProductoResponse> listaProductoResponses = productoService.findAll().stream().map(this::mapToPojo)
				.collect(Collectors.toList());
		return ResponseEntity.ok(listaProductoResponses);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{idProducto}")
	public ResponseEntity<Object> getProduct(@PathVariable Long idProducto) {
		Producto producto = productoService.findById(idProducto);
		if (producto == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(modelMapper.map(producto, ProductoResponse.class));

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{idProducto}")
	public ResponseEntity<Void> deleteProduc(@PathVariable Long idProducto) {
		productoService.delete(idProducto);
		return ResponseEntity.noContent().build();
	}

	public ProductoResponse mapToPojo(Producto producto) {
		ProductoResponse productoResponse = modelMapper.map(producto, ProductoResponse.class);
		return productoResponse;
	}

}
