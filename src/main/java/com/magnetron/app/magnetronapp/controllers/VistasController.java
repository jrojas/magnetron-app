package com.magnetron.app.magnetronapp.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.magnetron.app.magnetronapp.dto.PersonaFacturadoResponse;
import com.magnetron.app.magnetronapp.dto.PersonaProductoCostosoResponse;
import com.magnetron.app.magnetronapp.dto.ProductoMasFacturadoResponse;
import com.magnetron.app.magnetronapp.dto.ProductosPorUtilidadResponse;
import com.magnetron.app.magnetronapp.services.IFacturaService;

@RestController
@RequestMapping(value = "api/vistas")
public class VistasController {

	@Autowired
	IFacturaService facturaService;

	@GetMapping("/persona-facturado")
	public ResponseEntity<List<PersonaFacturadoResponse>> getProducts() {
		List<PersonaFacturadoResponse> listaResp = facturaService.consultarVistaPersonaFacturado();
		return ResponseEntity.ok(listaResp);
	}

	@GetMapping("/persona-prod-costoso")
	public ResponseEntity<List<PersonaProductoCostosoResponse>> getPersonaProdcutCostoso() {
		List<PersonaProductoCostosoResponse> listaResp = facturaService.consultarVistaPersonaProductoMasValioso();
		return ResponseEntity.ok(listaResp);
	}

	@GetMapping("/prod-mas-facturado")
	public ResponseEntity<List<ProductoMasFacturadoResponse>> getProdcutMasFacturado() {
		List<ProductoMasFacturadoResponse> listaResp = facturaService.consultarVistaProductosMasFacturados();
		return ResponseEntity.ok(listaResp);
	}

	@GetMapping("/prod-por-utilidad")
	public ResponseEntity<List<ProductosPorUtilidadResponse>> getProductoPorUtilidad() {
		List<ProductosPorUtilidadResponse> listaResp = facturaService.consultarVistaProductosPorUtilidad();
		return ResponseEntity.ok(listaResp);
	}

}
