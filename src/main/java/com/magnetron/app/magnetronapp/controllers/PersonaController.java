package com.magnetron.app.magnetronapp.controllers;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.magnetron.app.magnetronapp.dto.MessageResponse;
import com.magnetron.app.magnetronapp.dto.PersonaRequest;
import com.magnetron.app.magnetronapp.dto.PersonaResponse;
import com.magnetron.app.magnetronapp.entities.Persona;
import com.magnetron.app.magnetronapp.exceptions.ExcepcionApi;
import com.magnetron.app.magnetronapp.services.IPersonaService;

@RestController
@RequestMapping(value = "api/personas")
public class PersonaController {

	@Autowired
	IPersonaService personaService;

	@Autowired
	ModelMapper modelMapper;

	@RequestMapping(method = RequestMethod.POST, value = "")
	public ResponseEntity<Object> createPersona(@Valid @RequestBody PersonaRequest persona) {
		try {

			PersonaResponse per = modelMapper.map(personaService.save(persona), PersonaResponse.class);

			return new ResponseEntity<>(per, HttpStatus.CREATED);
		} catch (ExcepcionApi e) {
			return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("")
	@RequestMapping(method = RequestMethod.GET, value = "")
	public ResponseEntity<List<PersonaResponse>> getPersons() {
		List<PersonaResponse> listaPersonasResponses = personaService.findAll().stream().map(this::mapToPojo)
				.collect(Collectors.toList());
		return ResponseEntity.ok(listaPersonasResponses);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{idPerson}")
	public ResponseEntity<Object> getPerson(@PathVariable Long idPerson) {
		Persona persona = personaService.findById(idPerson);
		if (persona == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return ResponseEntity.ok(modelMapper.map(persona, PersonaResponse.class));

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{idPerson}")
	public ResponseEntity<Void> deletePerson(@PathVariable Long idPerson) {
		personaService.delete(idPerson);
		return ResponseEntity.noContent().build();
	}

	public PersonaResponse mapToPojo(Persona persona) {
		PersonaResponse personaResponse = modelMapper.map(persona, PersonaResponse.class);
		return personaResponse;
	}

}
