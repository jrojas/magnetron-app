package com.magnetron.app.magnetronapp.controllers;

import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.magnetron.app.magnetronapp.dto.FacturaRequest;
import com.magnetron.app.magnetronapp.dto.FacturaResponse;
import com.magnetron.app.magnetronapp.dto.MessageResponse;
import com.magnetron.app.magnetronapp.exceptions.ExcepcionApi;
import com.magnetron.app.magnetronapp.services.IFacturaService;

@RestController
@RequestMapping(value = "api/facturas")
public class FacturaController {

	@Autowired
	IFacturaService facturaService;

	@Autowired
	ModelMapper modelMapper;

	@RequestMapping(method = RequestMethod.POST, value = "")
	public ResponseEntity<Object> createFactura(@Valid @RequestBody FacturaRequest factura) {
		try {
			FacturaResponse res = facturaService.save(factura);
			return new ResponseEntity<>(res, HttpStatus.CREATED);
		} catch (ExcepcionApi e) {
			return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("")
	@RequestMapping(method = RequestMethod.GET, value = "")
	public ResponseEntity<List<FacturaResponse>> getFacturas() {
		List<FacturaResponse> listafacturas = facturaService.findAll();
		return ResponseEntity.ok(listafacturas);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/{idFactura}")
	public ResponseEntity<Object> getFactura(@PathVariable Long idFactura) {
		try {
			FacturaResponse res = facturaService.findById(idFactura);
			return ResponseEntity.ok(res);

		} catch (ExcepcionApi e) {
			return new ResponseEntity<>(new MessageResponse(e.getMessage()), HttpStatus.BAD_REQUEST);
		}

	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/{idFactura}")
	public ResponseEntity<Void> deleteFactura(@PathVariable Long idFactura) {
		facturaService.delete(idFactura);
		return ResponseEntity.noContent().build();
	}

}
