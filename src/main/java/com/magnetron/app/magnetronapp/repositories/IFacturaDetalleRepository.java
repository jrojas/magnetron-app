package com.magnetron.app.magnetronapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magnetron.app.magnetronapp.entities.FacturaDetalle;

public interface IFacturaDetalleRepository extends JpaRepository<FacturaDetalle, Long> {

}
