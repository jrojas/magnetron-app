package com.magnetron.app.magnetronapp.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.magnetron.app.magnetronapp.entities.FacturaEncabezado;

public interface IFacturaEncabezadoRepository extends JpaRepository<FacturaEncabezado, Long> {

	@Query(value = "SELECT * FROM V_Persona_Facturado", nativeQuery = true)
	List<Object[]> findViewPersonaFacturada();
	
	@Query(value = "SELECT * FROM V_Persona_Prod_Mas_Costoso", nativeQuery = true)
	List<Object[]> findViewPersonaProductoMasCostoso();
	
	@Query(value = "SELECT * FROM V_Productos_Mas_Facturados", nativeQuery = true)
	List<Object[]> findViewProductoMasFacturado();
	
	@Query(value = "SELECT * FROM V_Productos_Por_utilidad", nativeQuery = true)
	List<Object[]> findViewProductoPorUtilidad();
}
