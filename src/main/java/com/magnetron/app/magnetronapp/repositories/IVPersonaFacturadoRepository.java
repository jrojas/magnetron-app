package com.magnetron.app.magnetronapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magnetron.app.magnetronapp.entities.VPersonaFacturado;

public interface IVPersonaFacturadoRepository extends JpaRepository<VPersonaFacturado, Long> {

}
