package com.magnetron.app.magnetronapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magnetron.app.magnetronapp.entities.Persona;

public interface IPersonaRepository extends JpaRepository<Persona, Long> {

}
