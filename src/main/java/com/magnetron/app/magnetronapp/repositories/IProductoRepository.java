package com.magnetron.app.magnetronapp.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magnetron.app.magnetronapp.entities.Producto;

public interface IProductoRepository extends JpaRepository<Producto, Long> {

}
