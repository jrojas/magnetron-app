package com.magnetron.app.magnetronapp.services;

import java.util.List;

import com.magnetron.app.magnetronapp.dto.ProductoRequest;
import com.magnetron.app.magnetronapp.entities.Producto;
import com.magnetron.app.magnetronapp.exceptions.ExcepcionApi;

public interface IProductoService {
	
	public List<Producto> findAll();

	public Producto save(ProductoRequest producto) throws ExcepcionApi;

	public Producto findById(Long idProducto);

	public void delete(Long idProducto);

}
