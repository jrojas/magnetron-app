package com.magnetron.app.magnetronapp.services.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magnetron.app.magnetronapp.dto.FacturaDetalleRequest;
import com.magnetron.app.magnetronapp.dto.FacturaDetalleResponse;
import com.magnetron.app.magnetronapp.dto.FacturaRequest;
import com.magnetron.app.magnetronapp.dto.FacturaResponse;
import com.magnetron.app.magnetronapp.dto.PersonaFacturadoResponse;
import com.magnetron.app.magnetronapp.dto.PersonaProductoCostosoResponse;
import com.magnetron.app.magnetronapp.dto.PersonaResponse;
import com.magnetron.app.magnetronapp.dto.ProductoMasFacturadoResponse;
import com.magnetron.app.magnetronapp.dto.ProductoResponse;
import com.magnetron.app.magnetronapp.dto.ProductosPorUtilidadResponse;
import com.magnetron.app.magnetronapp.entities.FacturaDetalle;
import com.magnetron.app.magnetronapp.entities.FacturaEncabezado;
import com.magnetron.app.magnetronapp.exceptions.ExcepcionApi;
import com.magnetron.app.magnetronapp.repositories.IFacturaDetalleRepository;
import com.magnetron.app.magnetronapp.repositories.IFacturaEncabezadoRepository;
import com.magnetron.app.magnetronapp.repositories.IPersonaRepository;
import com.magnetron.app.magnetronapp.repositories.IProductoRepository;
import com.magnetron.app.magnetronapp.services.IFacturaService;

@Service
public class FacturaServiceImpl implements IFacturaService {

	@Autowired
	private IPersonaRepository personaRepository;

	@Autowired
	private IProductoRepository productoRepository;

	@Autowired
	private IFacturaDetalleRepository facturaDetalleRepository;

	@Autowired
	private IFacturaEncabezadoRepository facturaEncabezadoRepository;

	@Autowired
	ModelMapper modelMapper;

	@Override
	public List<FacturaResponse> findAll() {
		List<FacturaResponse> listaFacturas = facturaEncabezadoRepository.findAll().stream().map(this::mapperResponse)
				.collect(Collectors.toList());
		return listaFacturas;
	}

	@Override
	public FacturaResponse save(FacturaRequest factura) throws ExcepcionApi {
		// Validar la persona
		if (factura.getIdPersona() == null || personaRepository.findById(factura.getIdPersona()).orElse(null) == null) {
			throw new ExcepcionApi("Id Persona no existe");
		}
		// Validar Detalle validando producto
		if (factura.getDetallesFactura() != null && !factura.getDetallesFactura().isEmpty()) {
			for (FacturaDetalleRequest detalle : factura.getDetallesFactura()) {
				if (detalle.getProdId() == null
						|| productoRepository.findById(detalle.getProdId()).orElse(null) == null) {
					throw new ExcepcionApi("Id Producto en el detalle no existe");
				}
			}
		}
		FacturaEncabezado facturaEncabezado = factura.toEntity();
		facturaEncabezado = facturaEncabezadoRepository.save(facturaEncabezado);

		if (factura.getDetallesFactura() != null && !factura.getDetallesFactura().isEmpty()) {
			for (FacturaDetalleRequest detalle : factura.getDetallesFactura()) {
				if (detalle.getProdId() != null) {
					FacturaDetalle facturaDetalle = detalle.toEntity(facturaEncabezado);
					facturaDetalle = facturaDetalleRepository.save(facturaDetalle);
				}
			}
		}
		facturaEncabezado= facturaEncabezadoRepository.findById(facturaEncabezado.getFactEncId()).orElse(null);
		return mapperResponse(facturaEncabezado);
	}

	@Override
	public FacturaResponse findById(Long idFactura) throws ExcepcionApi {
		FacturaEncabezado fac = facturaEncabezadoRepository.findById(idFactura).orElse(null);
		if (fac == null) {
			throw new ExcepcionApi("Id Factura No Existe");
		}
		return mapperResponse(fac);
	}

	@Override
	public void delete(Long idFactura) {
		facturaEncabezadoRepository.deleteById(idFactura);

	}

	private FacturaResponse mapperResponse(FacturaEncabezado facturaEncabezado) {

		PersonaResponse persona = modelMapper.map(facturaEncabezado.getPersona(), PersonaResponse.class);

		List<FacturaDetalleResponse> detallesFactura = new ArrayList<>();
		if (facturaEncabezado.getFacturaDetalles() != null && !facturaEncabezado.getFacturaDetalles().isEmpty()) {
			for (FacturaDetalle facDet : facturaEncabezado.getFacturaDetalles()) {
				ProductoResponse producto = modelMapper.map(facDet.getProducto(), ProductoResponse.class);
				FacturaDetalleResponse facDetRes = new FacturaDetalleResponse();
				facDetRes.setCantidad(facDet.getFacDetalleCantidad());
				facDetRes.setLinea(facDet.getFacDetalleLinea());
				facDetRes.setProducto(producto);
				detallesFactura.add(facDetRes);
			}
		}

		FacturaResponse facturaResponse = new FacturaResponse();
		facturaResponse.setFactEncFecha(facturaEncabezado.getFactEncFecha());
		facturaResponse.setFactEncNumero(facturaEncabezado.getFactEncNumero());
		facturaResponse.setIdFactura(facturaEncabezado.getFactEncId());
		facturaResponse.setPersona(persona);
		facturaResponse.setDetallesFactura(detallesFactura);
		return facturaResponse;

	}

	@Override
	public List<PersonaFacturadoResponse> consultarVistaPersonaFacturado() {
		 List<Object[]> resultList =  facturaEncabezadoRepository.findViewPersonaFacturada();
		 List<PersonaFacturadoResponse> listaResponse = new ArrayList<>();
	        for (Object[] result : resultList) {
	        	PersonaFacturadoResponse res = new PersonaFacturadoResponse();
	        	res.setIdPersona((Long) result[0]);
	        	res.setFacturado((BigDecimal) result[1]);
	        	listaResponse.add(res);
	        }
		 return listaResponse;
	}

	@Override
	public List<PersonaProductoCostosoResponse> consultarVistaPersonaProductoMasValioso() {
		List<Object[]> resultList =  facturaEncabezadoRepository.findViewPersonaProductoMasCostoso();
		 List<PersonaProductoCostosoResponse> listaResponse = new ArrayList<>();
	        for (Object[] result : resultList) {
	        	PersonaProductoCostosoResponse res = new PersonaProductoCostosoResponse();	        	
	        	res.setIdPersona((Long) result[0]);
	        	res.setDocumentoPersona(result[1].toString());
	        	res.setNombrePersona(result[2].toString());
	        	res.setApellidoPersona(result[3].toString());
	        	listaResponse.add(res);
	        }
		 return listaResponse;
	
	}

	@Override
	public List<ProductoMasFacturadoResponse> consultarVistaProductosMasFacturados() {
		List<Object[]> resultList =  facturaEncabezadoRepository.findViewProductoMasFacturado();
		 List<ProductoMasFacturadoResponse> listaResponse = new ArrayList<>();
	        for (Object[] result : resultList) {
	        	ProductoMasFacturadoResponse res = new ProductoMasFacturadoResponse();
	        	res.setIdProducto((Long) result[0]);
	        	res.setDescripcionProducto(result[1].toString());
	        	res.setCantidadFacturada((BigDecimal) result[2]);
	        	listaResponse.add(res);
	        }
		 return listaResponse;
	}

	@Override
	public List<ProductosPorUtilidadResponse> consultarVistaProductosPorUtilidad() {
		List<Object[]> resultList =  facturaEncabezadoRepository.findViewProductoPorUtilidad();
		 List<ProductosPorUtilidadResponse> listaResponse = new ArrayList<>();
	        for (Object[] result : resultList) {
	        	ProductosPorUtilidadResponse res = new ProductosPorUtilidadResponse();
	        	res.setIdProducto((Long) result[0]);
	        	res.setDescripcionProducto(result[1].toString());
	        	res.setUtilidad((Long) result[2]);
	        	listaResponse.add(res);
	        }
		 return listaResponse;
	}

}
