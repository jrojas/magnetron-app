package com.magnetron.app.magnetronapp.services;

import java.util.List;

import com.magnetron.app.magnetronapp.dto.PersonaRequest;
import com.magnetron.app.magnetronapp.entities.Persona;
import com.magnetron.app.magnetronapp.exceptions.ExcepcionApi;

public interface IPersonaService {

	public List<Persona> findAll();

	public Persona save(PersonaRequest persona) throws ExcepcionApi;

	public Persona findById(Long idUser);

	public void delete(Long idUser);

}
