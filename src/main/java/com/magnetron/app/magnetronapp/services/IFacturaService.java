package com.magnetron.app.magnetronapp.services;

import java.util.List;

import com.magnetron.app.magnetronapp.dto.FacturaRequest;
import com.magnetron.app.magnetronapp.dto.FacturaResponse;
import com.magnetron.app.magnetronapp.dto.PersonaFacturadoResponse;
import com.magnetron.app.magnetronapp.dto.PersonaProductoCostosoResponse;
import com.magnetron.app.magnetronapp.dto.ProductoMasFacturadoResponse;
import com.magnetron.app.magnetronapp.dto.ProductosPorUtilidadResponse;
import com.magnetron.app.magnetronapp.exceptions.ExcepcionApi;

public interface IFacturaService {

	public List<FacturaResponse> findAll();

	public FacturaResponse save(FacturaRequest factura) throws ExcepcionApi;

	public FacturaResponse findById(Long idFactura) throws ExcepcionApi;

	public void delete(Long idFactura);

	public List<PersonaFacturadoResponse> consultarVistaPersonaFacturado();

	public List<PersonaProductoCostosoResponse> consultarVistaPersonaProductoMasValioso();

	public List<ProductoMasFacturadoResponse> consultarVistaProductosMasFacturados();

	public List<ProductosPorUtilidadResponse> consultarVistaProductosPorUtilidad();

}
