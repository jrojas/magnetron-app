package com.magnetron.app.magnetronapp.services.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magnetron.app.magnetronapp.dto.PersonaRequest;
import com.magnetron.app.magnetronapp.entities.Persona;
import com.magnetron.app.magnetronapp.exceptions.ExcepcionApi;
import com.magnetron.app.magnetronapp.repositories.IPersonaRepository;
import com.magnetron.app.magnetronapp.services.IPersonaService;

@Service
public class PersonaServiceImpl implements IPersonaService {

	private static final Logger log = LoggerFactory.getLogger(PersonaServiceImpl.class);

	@Autowired
	private IPersonaRepository personaRepository;

	@Autowired
	ModelMapper modelMapper;

	@Override
	public List<Persona> findAll() {
		return personaRepository.findAll();
	}

	@Override
	public Persona save(PersonaRequest persona) throws ExcepcionApi {
		Persona per = persona.toEntity();
		return personaRepository.save(per);
	}

	@Override
	public Persona findById(Long idUser) {
		return personaRepository.findById(idUser).orElse(null);
	}

	@Override
	public void delete(Long idUser) {
		personaRepository.deleteById(idUser);
	}

}
