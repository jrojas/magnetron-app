package com.magnetron.app.magnetronapp.services.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magnetron.app.magnetronapp.dto.ProductoRequest;
import com.magnetron.app.magnetronapp.entities.Producto;
import com.magnetron.app.magnetronapp.exceptions.ExcepcionApi;
import com.magnetron.app.magnetronapp.repositories.IProductoRepository;
import com.magnetron.app.magnetronapp.services.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService {

	private static final Logger log = LoggerFactory.getLogger(ProductoServiceImpl.class);

	@Autowired
	private IProductoRepository productoRepository;

	@Autowired
	ModelMapper modelMapper;

	@Override
	public List<Producto> findAll() {
		return productoRepository.findAll();
	}

	@Override
	public Producto save(ProductoRequest producto) throws ExcepcionApi {
		Producto pro = producto.toEntity();
		return productoRepository.save(pro);
	}

	@Override
	public Producto findById(Long idProducto) {
		return productoRepository.findById(idProducto).orElse(null);
	}

	@Override
	public void delete(Long idProducto) {
		productoRepository.deleteById(idProducto);
	}

}
