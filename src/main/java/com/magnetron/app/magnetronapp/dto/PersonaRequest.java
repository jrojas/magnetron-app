package com.magnetron.app.magnetronapp.dto;

import java.io.Serializable;

import com.magnetron.app.magnetronapp.entities.Persona;

public class PersonaRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String nombre;

	private String apellido;

	private String tipoDocumento;

	private String documento;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public Persona toEntity() {
		Persona per = new Persona();
		per.setPerApellido(apellido);
		per.setPerDocumento(documento);
		per.setPerNombre(nombre);
		per.setPerTipoDocumento(tipoDocumento);
		return per;

	}

}
