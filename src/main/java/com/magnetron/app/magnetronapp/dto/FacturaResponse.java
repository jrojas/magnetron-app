package com.magnetron.app.magnetronapp.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FacturaResponse implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private Long idFactura;
	
	private PersonaResponse persona;
	
	private List<FacturaDetalleResponse> detallesFactura=new ArrayList<>();
	
	private Long factEncNumero;

	private Date factEncFecha;

	public Long getIdFactura() {
		return idFactura;
	}
	
	public void setIdFactura(Long idFactura) {
		this.idFactura = idFactura;
	}

	public PersonaResponse getPersona() {
		return persona;
	}

	public void setPersona(PersonaResponse persona) {
		this.persona = persona;
	}

	public List<FacturaDetalleResponse> getDetallesFactura() {
		return detallesFactura;
	}

	public void setDetallesFactura(List<FacturaDetalleResponse> detallesFactura) {
		this.detallesFactura = detallesFactura;
	}

	public Long getFactEncNumero() {
		return factEncNumero;
	}

	public void setFactEncNumero(Long factEncNumero) {
		this.factEncNumero = factEncNumero;
	}

	public Date getFactEncFecha() {
		return factEncFecha;
	}

	public void setFactEncFecha(Date factEncFecha) {
		this.factEncFecha = factEncFecha;
	}

}
