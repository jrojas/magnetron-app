package com.magnetron.app.magnetronapp.dto;

public class PersonaResponse extends PersonaRequest {

	private static final long serialVersionUID = 1L;

	private Long idPersona;

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

}
