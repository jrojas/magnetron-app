package com.magnetron.app.magnetronapp.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.magnetron.app.magnetronapp.entities.FacturaEncabezado;
import com.magnetron.app.magnetronapp.entities.Persona;

public class FacturaRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idPersona;

	private Long factEncNumero;

	private Date factEncFecha;
	
	private List<FacturaDetalleRequest> detallesFactura=new ArrayList<>();	
	

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public Long getFactEncNumero() {
		return factEncNumero;
	}

	public void setFactEncNumero(Long factEncNumero) {
		this.factEncNumero = factEncNumero;
	}

	public Date getFactEncFecha() {
		return factEncFecha;
	}

	public void setFactEncFecha(Date factEncFecha) {
		this.factEncFecha = factEncFecha;
	}

	public List<FacturaDetalleRequest> getDetallesFactura() {
		return detallesFactura;
	}

	public void setDetallesFactura(List<FacturaDetalleRequest> detallesFactura) {
		this.detallesFactura = detallesFactura;
	}
	
	public FacturaEncabezado toEntity() {
		FacturaEncabezado facturaEncabezado=new FacturaEncabezado();
		facturaEncabezado.setFactEncFecha(new Date());
		facturaEncabezado.setFactEncId(factEncNumero);
		facturaEncabezado.setFactEncNumero(factEncNumero);
		Persona per=new Persona();
		per.setPerId(idPersona);
		facturaEncabezado.setPersona(per);
		return facturaEncabezado;
		
	}

	

}
