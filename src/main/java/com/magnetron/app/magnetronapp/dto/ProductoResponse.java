package com.magnetron.app.magnetronapp.dto;

public class ProductoResponse extends ProductoRequest {

	private static final long serialVersionUID = 1L;

	private Long idProducto;

	public Long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	

}
