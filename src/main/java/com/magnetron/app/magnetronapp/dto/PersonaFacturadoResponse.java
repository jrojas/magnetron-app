package com.magnetron.app.magnetronapp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class PersonaFacturadoResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long idPersona;
	
	private BigDecimal facturado;

	public Long getIdPersona() {
		return idPersona;
	}

	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}

	public BigDecimal getFacturado() {
		return facturado;
	}

	public void setFacturado(BigDecimal facturado) {
		this.facturado = facturado;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

}
