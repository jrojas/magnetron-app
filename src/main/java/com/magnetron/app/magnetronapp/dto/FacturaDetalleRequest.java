package com.magnetron.app.magnetronapp.dto;

import java.io.Serializable;

import com.magnetron.app.magnetronapp.entities.FacturaDetalle;
import com.magnetron.app.magnetronapp.entities.FacturaEncabezado;
import com.magnetron.app.magnetronapp.entities.Producto;

public class FacturaDetalleRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long linea;

	private Long cantidad;

	private Long prodId;

	

	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}
	
	public FacturaDetalle toEntity(FacturaEncabezado facturaEncabezado) {
		Producto producto=new Producto();
		producto.setProdId(prodId);
		FacturaDetalle facturaDetalle=new FacturaDetalle();
		facturaDetalle.setFacDetalleCantidad(cantidad);
		facturaDetalle.setFacDetalleLinea(linea);
		facturaDetalle.setProducto(producto);
		facturaDetalle.setFacturaEncabezado(facturaEncabezado);
		return facturaDetalle;
	}

}
