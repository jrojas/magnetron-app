package com.magnetron.app.magnetronapp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProductoMasFacturadoResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	private Long idProducto;

	private String descripcionProducto;

	private BigDecimal cantidadFacturada;

	public Long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	public String getDescripcionProducto() {
		return descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public BigDecimal getCantidadFacturada() {
		return cantidadFacturada;
	}

	public void setCantidadFacturada(BigDecimal cantidadFacturada) {
		this.cantidadFacturada = cantidadFacturada;
	}

}
