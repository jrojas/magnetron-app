package com.magnetron.app.magnetronapp.dto;

import java.io.Serializable;
import java.math.BigDecimal;

public class ProductosPorUtilidadResponse 
implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long idProducto;
	
	private String descripcionProducto;
	
	private Long utilidad;

	public Long getIdProducto() {
		return idProducto;
	}

	public void setIdProducto(Long idProducto) {
		this.idProducto = idProducto;
	}

	public String getDescripcionProducto() {
		return descripcionProducto;
	}

	public void setDescripcionProducto(String descripcionProducto) {
		this.descripcionProducto = descripcionProducto;
	}

	public Long getUtilidad() {
		return utilidad;
	}

	public void setUtilidad(Long utilidad) {
		this.utilidad = utilidad;
	}
	

}
