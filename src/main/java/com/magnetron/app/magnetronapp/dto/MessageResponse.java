package com.magnetron.app.magnetronapp.dto;

import java.io.Serializable;

public class MessageResponse implements Serializable {

	private static final long serialVersionUID = 1L;

	public MessageResponse(String message) {
		super();
		this.message = message;
	}

	String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
