package com.magnetron.app.magnetronapp.dto;

import java.io.Serializable;

public class FacturaDetalleResponse  implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long linea;

	private Long cantidad;

	private ProductoResponse producto;

	public Long getLinea() {
		return linea;
	}

	public void setLinea(Long linea) {
		this.linea = linea;
	}

	public Long getCantidad() {
		return cantidad;
	}

	public void setCantidad(Long cantidad) {
		this.cantidad = cantidad;
	}

	public ProductoResponse getProducto() {
		return producto;
	}

	public void setProducto(ProductoResponse producto) {
		this.producto = producto;
	}


}
