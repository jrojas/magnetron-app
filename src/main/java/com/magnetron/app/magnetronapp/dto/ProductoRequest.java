package com.magnetron.app.magnetronapp.dto;

import java.io.Serializable;

import com.magnetron.app.magnetronapp.entities.Persona;
import com.magnetron.app.magnetronapp.entities.Producto;

public class ProductoRequest implements Serializable {

	private static final long serialVersionUID = 1L;

	private String description;

	private Long precio;

	private Long costo;

	private String um;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Long getPrecio() {
		return precio;
	}

	public void setPrecio(Long precio) {
		this.precio = precio;
	}

	public Long getCosto() {
		return costo;
	}

	public void setCosto(Long costo) {
		this.costo = costo;
	}

	public String getUm() {
		return um;
	}

	public void setUm(String um) {
		this.um = um;
	}
	
	public Producto toEntity() {
		Producto pro = new Producto();
		pro.setProdCosto(costo);
		pro.setProdDescription(description);
		pro.setProdPrecio(precio);
		pro.setProdUm(um);		
		return pro;

	}

}
