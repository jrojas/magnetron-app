package com.magnetron.app.magnetronapp.dto;

import java.io.Serializable;

public class PersonaProductoCostosoResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long idPersona;	
	private String documentoPersona;
	private String nombrePersona;
	private String apellidoPersona;
	
	public Long getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	public String getDocumentoPersona() {
		return documentoPersona;
	}
	public void setDocumentoPersona(String documentoPersona) {
		this.documentoPersona = documentoPersona;
	}
	public String getNombrePersona() {
		return nombrePersona;
	}
	public void setNombrePersona(String nombrePersona) {
		this.nombrePersona = nombrePersona;
	}
	public String getApellidoPersona() {
		return apellidoPersona;
	}
	public void setApellidoPersona(String apellidoPersona) {
		this.apellidoPersona = apellidoPersona;
	}
	

}
