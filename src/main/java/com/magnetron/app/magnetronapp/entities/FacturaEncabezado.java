package com.magnetron.app.magnetronapp.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Temporal;
import jakarta.persistence.TemporalType;

@Entity
@Table(name = "Fact_Encabezado")
public class FacturaEncabezado implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FEnc_Id")
    private Long factEncId;
	
	@Column(name = "FEnc_Numero")
    private Long factEncNumero;
	
	@Temporal(TemporalType.DATE)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "FEnc_Fecha")
    private Date factEncFecha;
	
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "per_id")
	private Persona persona;
	
	
	@OneToMany(mappedBy = "facturaEncabezado", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<FacturaDetalle> facturaDetalles;
	
	
	

	public Long getFactEncNumero() {
		return factEncNumero;
	}


	public void setFactEncNumero(Long factEncNumero) {
		this.factEncNumero = factEncNumero;
	}


	public Date getFactEncFecha() {
		return factEncFecha;
	}


	public void setFactEncFecha(Date factEncFecha) {
		this.factEncFecha = factEncFecha;
	}


	public Persona getPersona() {
		return persona;
	}


	public void setPersona(Persona persona) {
		this.persona = persona;
	}


	public Long getFactEncId() {
		return factEncId;
	}


	public void setFactEncId(Long factEncId) {
		this.factEncId = factEncId;
	}


	public List<FacturaDetalle> getFacturaDetalles() {
		return facturaDetalles;
	}


	public void setFacturaDetalles(List<FacturaDetalle> facturaDetalles) {
		this.facturaDetalles = facturaDetalles;
	}
	

}
