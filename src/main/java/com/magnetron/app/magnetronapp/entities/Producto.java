package com.magnetron.app.magnetronapp.entities;

import java.io.Serializable;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "Producto")
public class Producto implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Prod_Id")
    private Long prodId;
	
	@Column(name = "Prod_Description")
	private String prodDescription;
	
	@Column(name = "Prod_Precio")
	private Long prodPrecio;
	
	@Column(name = "Prod_Costo")
	private Long prodCosto;
	
	@Column(name = "Prod_UM")
	private String prodUm;
	


	public Long getProdId() {
		return prodId;
	}

	public void setProdId(Long prodId) {
		this.prodId = prodId;
	}

	public String getProdDescription() {
		return prodDescription;
	}

	public void setProdDescription(String prodDescription) {
		this.prodDescription = prodDescription;
	}

	public Long getProdPrecio() {
		return prodPrecio;
	}

	public void setProdPrecio(Long prodPrecio) {
		this.prodPrecio = prodPrecio;
	}

	public Long getProdCosto() {
		return prodCosto;
	}

	public void setProdCosto(Long prodCosto) {
		this.prodCosto = prodCosto;
	}

	public String getProdUm() {
		return prodUm;
	}

	public void setProdUm(String prodUm) {
		this.prodUm = prodUm;
	}

}
