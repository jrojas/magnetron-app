package com.magnetron.app.magnetronapp.entities;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;

@Entity
@Table(name = "Fact_detalle")
public class FacturaDetalle implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "FDet_Id")
    private Long facDetalleId;
	
	@Column(name = "FDet_Linea")
    private Long facDetalleLinea;
	
	@Column(name = "FDet_Cantidad")
    private Long facDetalleCantidad;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "Prod_Id")
	private Producto producto;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FEnc_Id")
	private FacturaEncabezado facturaEncabezado;

	public Long getFacDetalleId() {
		return facDetalleId;
	}

	public void setFacDetalleId(Long facDetalleId) {
		this.facDetalleId = facDetalleId;
	}

	public Long getFacDetalleLinea() {
		return facDetalleLinea;
	}

	public void setFacDetalleLinea(Long facDetalleLinea) {
		this.facDetalleLinea = facDetalleLinea;
	}

	public Long getFacDetalleCantidad() {
		return facDetalleCantidad;
	}

	public void setFacDetalleCantidad(Long facDetalleCantidad) {
		this.facDetalleCantidad = facDetalleCantidad;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	public FacturaEncabezado getFacturaEncabezado() {
		return facturaEncabezado;
	}

	public void setFacturaEncabezado(FacturaEncabezado facturaEncabezado) {
		this.facturaEncabezado = facturaEncabezado;
	}

	
	

}
