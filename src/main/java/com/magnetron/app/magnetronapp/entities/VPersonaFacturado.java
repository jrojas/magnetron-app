package com.magnetron.app.magnetronapp.entities;

import org.hibernate.annotations.Immutable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name = "V_Persona_Facturado")
public class VPersonaFacturado {
	
	@Id
	@Column(name = "per_id")
	private Long perId;
	
	private Double  facturado;
	
	
	public Long getPerId() {
		return perId;
	}
	public void setPerId(Long perId) {
		this.perId = perId;
	}
	public Double  getFacturado() {
		return facturado;
	}
	public void setFacturado(Double  facturado) {
		this.facturado = facturado;
	}

}
