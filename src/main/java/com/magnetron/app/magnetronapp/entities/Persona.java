package com.magnetron.app.magnetronapp.entities;

import java.io.Serializable;
import java.util.List;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;

@Entity
@Table(name = "Persona")
public class Persona implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "Per_Id")
    private Long perId;
	
	@Column(name = "Per_Nombre")
    private String perNombre;
	
	@Column(name = "Per_Apellido")
    private String perApellido;
	
	@Column(name = "Per_TipoDocumento")
    private String perTipoDocumento;
	
	@Column(name = "Per_Documento")
    private String perDocumento;
	
	
	public Long getPerId() {
		return perId;
	}

	public void setPerId(Long perId) {
		this.perId = perId;
	}

	public String getPerNombre() {
		return perNombre;
	}

	public void setPerNombre(String perNombre) {
		this.perNombre = perNombre;
	}

	public String getPerApellido() {
		return perApellido;
	}

	public void setPerApellido(String perApellido) {
		this.perApellido = perApellido;
	}

	public String getPerTipoDocumento() {
		return perTipoDocumento;
	}

	public void setPerTipoDocumento(String perTipoDocumento) {
		this.perTipoDocumento = perTipoDocumento;
	}

	public String getPerDocumento() {
		return perDocumento;
	}

	public void setPerDocumento(String perDocumento) {
		this.perDocumento = perDocumento;
	}
    

}
