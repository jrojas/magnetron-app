package com.magnetron.app.magnetronapp.exceptions;

public class ExcepcionApi extends Exception {

	private static final long serialVersionUID = 1L;

	public ExcepcionApi(String message) {
		super(message);
	}

}
