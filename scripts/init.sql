-- Crear la base de datos
CREATE DATABASE IF NOT EXISTS databasefac;
USE databasefac;

-- Crear tabla persona
CREATE TABLE persona (
  per_id bigint NOT NULL AUTO_INCREMENT,
  per_apellido varchar(255) DEFAULT NULL,
  per_documento varchar(255) DEFAULT NULL,
  per_nombre varchar(255) DEFAULT NULL,
  per_tipo_documento varchar(255) DEFAULT NULL,
  PRIMARY KEY (per_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Insertar Datos Persona
INSERT INTO persona (per_id, per_apellido, per_documento, per_nombre, per_tipo_documento) VALUES
    (1, 'Roa', '123456','Andres', 'CC'),
    (2, 'Torres', '4454554','Jhon', 'CC'),
    (3, 'Granados', '1234454','Juan', 'CC'),
    (4, 'Arcila', '3332544','Sebas', 'CC'),
    (5, 'Duarte', '455554','Duan', 'CC');

-- Crear Tabla Producto
CREATE TABLE producto (
  prod_id bigint NOT NULL AUTO_INCREMENT,
  prod_costo bigint DEFAULT NULL,
  prod_description varchar(255) DEFAULT NULL,
  prod_precio bigint DEFAULT NULL,
  prod_um varchar(255) DEFAULT NULL,
  PRIMARY KEY (prod_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- Insertar Datos Producto
INSERT INTO producto(prod_id, prod_costo, prod_description,prod_precio, prod_um) VALUES
    (1,  2500 , 'Producto 1', 3000, 'UN1'),
    (2,  2500 , 'Producto 2', 2000, 'UN2'),
    (3,  3500 , 'Producto 3', 3000, 'UN3'),
    (4,  4500 , 'Producto 4', 4000, 'UN4'),
    (5,  5500 , 'Producto 5', 5000, 'UN5');

-- Crear Tabla  fact_encabezado
CREATE TABLE fact_encabezado (
  fenc_id bigint NOT NULL AUTO_INCREMENT,
  fenc_fecha date DEFAULT NULL,
  fenc_numero bigint DEFAULT NULL,
  per_id bigint DEFAULT NULL,
  PRIMARY KEY (fenc_id),
  KEY FKdtsjndd1peo2x0poy25x1qv2o (per_id),
  CONSTRAINT FKdtsjndd1peo2x0poy25x1qv2o FOREIGN KEY (per_id) REFERENCES persona (per_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO fact_encabezado(fenc_id, fenc_fecha, fenc_numero,per_id) VALUES
    (1,  '2024-01-15' , 12345, 1),
    (2,  '2024-01-15' , 12222, 1),
    (3,  '2024-01-15' , 13335, 1),
    (4,  '2024-01-15' , 144445, 2),
    (5,  '2024-01-15' , 15555, 2),
    (6,  '2024-01-15' , 166665, 2),
    (7,  '2024-01-15' , 1777, 3),
    (8,  '2024-01-15' , 188882, 3),
    (9,  '2024-01-15' , 199999, 3),
    (10,  '2024-01-15' , 11101010, 4);

-- Crear Tabla fact_detalle

CREATE TABLE fact_detalle (
  fdet_id bigint NOT NULL AUTO_INCREMENT,
  fdet_cantidad bigint DEFAULT NULL,
  fdet_linea bigint DEFAULT NULL,
  fenc_id bigint DEFAULT NULL,
  prod_id bigint DEFAULT NULL,
  PRIMARY KEY (fdet_id),
  KEY FK6e1p1x1wu0vhlu3i7uygcccxb (fenc_id),
  KEY FKngpot277ybtcl150oamqmlpqs (prod_id),
  CONSTRAINT FK6e1p1x1wu0vhlu3i7uygcccxb FOREIGN KEY (fenc_id) REFERENCES fact_encabezado (fenc_id),
  CONSTRAINT FKngpot277ybtcl150oamqmlpqs FOREIGN KEY (prod_id) REFERENCES producto (prod_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO fact_detalle(fdet_id, fdet_cantidad, fdet_linea,fenc_id, prod_id ) VALUES
    (1, 20, 1, 1, 1),
    (2, 20, 2, 1, 2),
    (3, 30, 3, 1, 3),
    (4, 40, 4, 2, 1),
    (5, 50, 5, 3, 2),
    (6, 60, 6, 3, 3),
    (7, 70, 7, 3, 1),    
    (8, 50, 5, 4, 4),
    (9, 60, 6, 4, 4),
    (10, 70, 7, 4, 5);
    
-- Creacion de las Vistas

-- Vista  V_Persona_Facturado
create or replace view V_Persona_Facturado as
select p.per_id , COALESCE(SUM(fd.fdet_cantidad*p2.prod_precio),0) facturado
from persona p 
LEFT JOIN  fact_encabezado fe on fe.per_id =p.per_id 
LEFT JOIN  fact_detalle fd on fd.fenc_id = fe.fenc_id
LEFT JOIN  producto p2 on p2.prod_id =fd.prod_id
group by p.per_id;

-- Vista  V_Persona_Prod_Mas_Costoso
create or replace view V_Persona_Prod_Mas_Costoso as
select DISTINCT p.per_id , p.per_documento , p.per_nombre , p.per_apellido
from persona p 
JOIN  fact_encabezado fe on fe.per_id =p.per_id 
JOIN  fact_detalle fd on fd.fenc_id = fe.fenc_id,
(
	select p2.prod_id  prod_id , max(p2.prod_costo) prod_costo
	from producto p2
	group by p2.prod_id
	order by 2 desc
	limit 1
) p3
where fd.prod_id=p3.prod_id;

-- Vista V_Productos_Mas_Facturados

create or replace view V_Productos_Mas_Facturados as
select p2.prod_id , p2.prod_description , sum(fd.fdet_cantidad) cantidad_facturada
from fact_encabezado fe  
JOIN  fact_detalle fd on fd.fenc_id = fe.fenc_id
JOIN  producto p2 on p2.prod_id =fd.prod_id
group by  p2.prod_id , p2.prod_description
order by 3 desc;

-- create view V_Productos_Por_utilidad as
create or replace view V_Productos_Por_utilidad as
select p2.prod_id , p2.prod_description , p2.prod_precio - p2.prod_costo utilidad
from fact_encabezado fe  
JOIN  fact_detalle fd on fd.fenc_id = fe.fenc_id
JOIN  producto p2 on p2.prod_id =fd.prod_id
group by  p2.prod_id , p2.prod_description
order by 3 desc;

